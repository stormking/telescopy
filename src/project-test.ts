import { Node } from "@stormking/datastructures";
import Project from "./project";
import Resource from "./resource";

export default class ProjectTest extends Project {

	protected results = new Set<Node>();

	protected async processNext() {
		let node: Node = this.queue.shift();
		let res = new Resource(this, node);
		await res.process();
		for (let link of node.getLinks()) {
			let connected = link.getTo();
			this.results.add(connected);
		}
		this.endResolve();
	}

	getResults() {
		return this.results;
	}

}
