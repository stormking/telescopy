import { expect } from 'chai';
import * as http from 'http';
import Project from '../../src/project';
import ProjectSettings from '../../src/project-settings';
import Debug from 'debug';
import { DONE, FAILED, URL as RES_URL } from '../../src/project-state';
const debug = Debug('test:http');

describe('Headers', () => {

	let server;
	let onRequest;

	let project: Project;
	let requestNum = 0;
	const requestTimeout = 10;
	let maxRequests = 3;

	before((done) => {
		server = http.createServer((req, res) => {
			// console.log(req.method, req.url);
			if (onRequest) onRequest(req, res);
			else res.end(500);
		});
		server.listen(8080, done);
	});
	after(() => {
		server.close();
	})

	describe('handle repeated time-outs', () => {

		before(() => {
			requestNum = 0;
			maxRequests = 3;
			onRequest = (req,res) => {
				requestNum += 1;
				let delay = requestNum < maxRequests ? requestTimeout * 10 : 0;
				debug('got request');
				setTimeout(() => {
					res.writeHead(200,{
						'Content-Type': 'text/html'
					});
					res.end('<p>hello</p>');
				}, delay);
			}
			project = new Project({
				local: '.',
				storage: 'memory',
				remote: 'http://localhost:8080',
				retryDelay(a) {
					debug('retry delay', a.attemptCount);
					if (a.attemptCount > maxRequests) return 0;
					return 1;
				},
				httpTimeouts: {
					request: requestTimeout
				}
			});
		});

		it('can fetch the url with 3 retries', async() => {
			debug('start request');
			let res = await project.fetchUrl('http://localhost:8080/index', '');
			debug('response done');
			expect(res.statusCode).to.equal(200);
			expect(res.body).to.include('hello');
			expect(requestNum).to.equal(maxRequests);
		});

	});

	describe('will stop after too many timeouts', () => {


		before(() => {
			requestNum = 0;
			maxRequests = 3;
			onRequest = (req,res) => {
				requestNum += 1;
				let delay = requestTimeout * 3;
				debug('got request');
				setTimeout(() => {
					res.writeHead(200,{
						'Content-Type': 'text/html'
					});
					res.end('<p>hello</p>');
				}, delay);
			}
			project = new Project({
				local: '.',
				storage: 'memory',
				remote: 'http://localhost:8080',
				retryDelay(a) {
					debug('retry delay', a.attemptCount);
					if (a.attemptCount > maxRequests) return 0;
					return 1;
				},
				httpTimeouts: {
					request: requestTimeout
				}
			});
		});

		it('can fetch the url with 3 retries', async() => {
			let thrown = false;
			try {
				debug('start request');
				let res = await project.fetchUrl('http://localhost:8080/index', '');
				debug('response done');
			} catch (e) {
				expect(e.message).to.include('Timeout');
				thrown = true;
			}
			expect(thrown).to.be.true;
			expect(requestNum).to.equal(maxRequests+1);
		});

	});

	describe('can download a previously unavailable resource', () => {

		describe('1st mirror', () => {

			before(() => {
				maxRequests = 1;
				onRequest = (req,res) => {
					if (req.url === '/') {
						res.writeHead(200, {
							'Content-Type': 'text/html'
						});
						res.end('<p>hello</p><script src="./test.js" />');
					} else {
						res.writeHead(500, {
							'Content-Type': 'text/html'
						});
						res.end();
					}
				}
				project = new Project({
					local: '/tmp/tcopy-test',
					cleanLocal: true,
					remote: 'http://localhost:8080',
					retryDelay(a) {
						debug('retry delay', a.attemptCount);
						if (a.attemptCount > maxRequests) return 0;
						return 1;
					},
				})
			})

			it('can run the project', async() => {
				await project.start();
				await project.endPromise;
			});

			it('has the correct project state', async() => {
				let entries = project.getState().getAll();
				expect(entries.count()).to.equal(2);
				for (let node of entries) {
					// console.log(node);
					if (node.getAttribute(RES_URL) === 'http://localhost:8080') {
						expect(node.getAttribute(DONE)).to.equal(true);
						expect(!!node.getAttribute(FAILED)).to.equal(false);
					} else {
						expect(node.getAttribute(FAILED)).to.equal(true);
					}
				}
			})

		})


		describe('2nd mirror', () => {

			before(() => {
				onRequest = (req,res) => {
					if (req.url === '/') {
						res.writeHead(200,{
							'Content-Type': 'text/html'
						});
						res.end('<p>hello</p><script src="./test.js" />');
					} else {
						res.writeHead(200,{
							'Content-Type': 'application/javascript'
						});
						res.end('var t = 5;');
					}
				}
				project = new Project({
					local: '/tmp/tcopy-test',
					remote: 'http://localhost:8080'
				})
			})

			it('can run the project', async() => {
				await project.start();
				await project.endPromise;
			});

			it('has the correct project state', async() => {
				let entries = project.getState().getAll();
				expect(entries.count()).to.equal(2);
				for (let node of entries) {
					// console.log(node);
					if (node.getAttribute(RES_URL) === 'http://localhost:8080') {
						expect(node.getAttribute(DONE)).to.equal(true);
						expect(!!node.getAttribute(FAILED)).to.equal(false);
					} else {
						expect(node.getAttribute(DONE)).to.equal(true);
						expect(!!node.getAttribute(FAILED)).to.equal(false);
					}
				}
			})

		})


	});




});
