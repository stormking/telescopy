import StaticServer from 'static-server';
import Project from '../../src/project';
import { expect } from 'chai';
import * as cheerio from 'cheerio';
import filter from '../../src/filter';
import { URL } from 'url';

describe('Telescopy Filters', () => {
	let server;
	let project;
	const local = '/tmp/tcopy-test/';
	async function getCheerio(f) {
		let buf = await project.getStorage().retrieve(f);
		const $ = cheerio.load(buf);
		return $;
	}
	before((done) => {
		server = new StaticServer({
			rootPath: `${__dirname}/../fixtures/server1/`,
			port: 8080,
			followSymlink: true
		});
		server.start(done);
	});

	after(() => {
		server.stop();
	});

	describe('filter out paths', () => {
		it('can run the project', async() => {
			project = new Project({
				remote: 'http://localhost:8080/index.html',
				local,
				cleanLocal: true,
				storage: 'file',
				urlFilter: [{
					type: 'path',
					match: false,
					test: '/\\/d\\//'
				}, true]
			});
			await project.start();
			await project.endPromise;
		});

		it('has written the index file', async() => {
			let has = await project.getStorage().has(('localhost/index.html'));
			expect(has).to.be.true;
		});

		it('has not written d/page0', async() => {
			let has = await project.getStorage().has(('localhost/d/page0.html'));
			expect(has).to.be.false;
		});

		it('has absolute url to d/page0.html', async() => {
			let $ = await getCheerio('localhost/index.html');
			let link = $('#l2');
			expect(link.length).to.equal(1);
			expect(link.attr('href')).to.equal('http://localhost:8080/d/page0.html');
		});
	});

	describe('remove filtered out tag', () => {
		it('can run the project', async() => {
			project = new Project({
				remote: 'http://localhost:8080/index.html',
				local,
				cleanLocal: true,
				storage: 'file',
				urlFilter: [{
					type: 'path',
					match: false,
					test: '/\\/(d|media)\\//'
				}, true],
				removeFilteredResource: true
			});
			await project.start();
			await project.endPromise;
		});

		it('has removed stylesheet and script', async() => {
			let $ = await getCheerio('localhost/index.html');
			let style = $('link[rel=stylesheet]');
			expect(style.length).to.equal(0);
			let script = $('script');
			expect(script.length).to.equal(0);
		});

	});

	describe('maximum crawling depth', () => {
		it('can run the project', async() => {
			project = new Project({
				remote: 'http://localhost:8080/index.html',
				local,
				cleanLocal: true,
				storage: 'file',
				maxCrawlDepth: 2
			});
			await project.start();
			await project.endPromise;
		});

		it('should not have downloaded page2', async() => {
			let has = await project.getStorage().has(('localhost/d/page2.html'));
			expect(has).to.be.false;
		});

		it('should have absolute link for not downloaded page2', async() => {
			let $ = await getCheerio('localhost/d/page1.html');
			let link = $('#p2');
			expect(link.length).to.equal(1);
			expect(link.attr('href')).to.equal('http://localhost:8080/d/page2.html?date=20150512');
		});
	});


	describe('More Filter Unit Tests', () => {

		describe('Query Filter', () => {

			let fn;
			before(() => {
				fn = filter([{
					type: 'query',
					match: true,
					key: 'archive'
				}, {
					type: 'query',
					match: true,
					comparison: '<',
					key: 'date',
					value: '2000'
				}, false])
			})

			it('should allow ?archive', () => {
				expect(fn(new URL('http://localhost/?archive'))).to.be.true;
			})

			it('should reject ?archives', () => {
				expect(fn(new URL('http://localhost/?archives'))).to.be.false;
			})

			it('should allow date before 2000', () => {
				expect(fn(new URL('http://localhost/?date=1540'))).to.be.true;
			})

			it('should reject date after 2000', () => {
				expect(fn(new URL('http://localhost/?date=2210'))).to.be.false;
			})

		})

		describe('Host', () => {

			let fn;
			before(() => {
				fn = filter([{
					type: 'host',
					match: true,
					test: '/\\.example\\.com$/'
				}, {
					type: 'host',
					match: true,
					value: 'example.org'
				}, false])
			})

			it('should allow ar.example.com', () => {
				expect(fn(new URL('http://ar.example.com/'))).to.be.true;
			})

			it('should allow example.org', () => {
				expect(fn(new URL('http://example.org/'))).to.be.true;
			})

			it('should allow example.eu', () => {
				expect(fn(new URL('http://example.eu/'))).to.be.false;
			})

		});

		describe('Path', () => {

			let fn;
			before(() => {
				fn = filter([{
					type: 'path',
					match: false,
					test: '/^\\/media\\//'
				}, true])
			})

			it('should reject media', () => {
				expect(fn(new URL('http://example.com/media/icon.png'))).to.be.false;
			})

			it('should allow index', () => {
				expect(fn(new URL('http://example.org/index'))).to.be.true;
			})

		})

	})


});
