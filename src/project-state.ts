import Project from './project';
import Path from 'path';
import { Graph, Node } from '@stormking/datastructures';
import Debug from 'debug';
const debug = Debug('tc-state');
import { NodeCollection } from '@stormking/datastructures/dist/node/lib/graph/collection';
import { createGzip, createGunzip } from 'zlib';

export const URL = 'url';
export const PATH = 'path';
export const MAX_DEPTH = 'maxDepth';
export const DONE = 'done';
export const MIME = 'mime';
export const FIRST_REF = 'firstReferer';
export const REMOVED = 'removed';
export const LAST_DOWNLOAD = 'lastDownload';
export const LAST_SEEN = 'lastSeen';
export const UPDATED_THIS_RUN = 'updatedThisRun';
export const ALLOWED = 'allowed';
export const FAILED = 'failed';
export const BASE_URL = 'baseUrl';
export const SKIP = 'skip';
export const QUEUED = 'queued';
export const LINK_USED = 'used';

export default class ProjectState {

	private index: Map<string, Node> = new Map();
	private urlFilter: Function;
	private project: Project;
	private graph: Graph;
	private useZip: boolean;

	constructor(project: Project) {
		this.project = project;
		this.urlFilter = project.urlFilter;
		this.graph = new Graph();
		this.useZip = !project.getNoDbCompress();
	}

	public async init() {
		let fileName = this.getDbFileName(false);
		let exists = await this.project.getStorage().has(fileName);
		if (!exists) {
			this.useZip = false;
			fileName = this.getDbFileName(true);
			exists = await this.project.getStorage().has(fileName);
		}
		if (exists) {
			let stream = this.project.getStorage().getReadStream(fileName);
			if (this.useZip) {
				stream = stream.pipe(createGunzip());
			}
			let stats = await this.graph.loadFromFile(stream);
			debug('loaded: ', stats);
			for (let node of this.graph.getCollection()) {
				node.setAttribute(DONE, false);
				node.setAttribute(SKIP, node.hasAttribute(LAST_DOWNLOAD) && !node.getAttribute(FAILED) && this.shouldSkipExisting(node));
				node.deleteAttribute(UPDATED_THIS_RUN);
				let depth = node.getAttribute(MAX_DEPTH);
				if (typeof depth === 'number') {
					node.deleteAttribute(MAX_DEPTH);
				}
				// let allowedNew = this.urlFilter(node.getAttribute(URL));
				// node.setAttribute(ALLOWED, allowedNew);
				this.index.set( node.getAttribute(URL), node );
				this.index.set( node.getAttribute(PATH), node );
				// console.log('loaded: ', node.getAttribute(URL),
				// 	Array.from(node.getLinks(l => l.getTo() === node))
				// 		.map((e) => e.getFrom().getAttribute(URL))
				// 		.join(', ')
				// );
			}
		}
	}

	public async save() {
		let fileName = this.getDbFileName(false);
		let output = this.project.getStorage().getWriteStream(fileName);
		if (!this.project.getNoDbCompress()) {
			let out = output;
			output = createGzip();
			output.pipe(out);
		}
		await this.graph.saveToFile(output);
	}

	protected getDbFileName(fallback=false) {
		let f = 'project.jsonnd';
		if (!this.project.getNoDbCompress() && !fallback) {
			f = `${f}.gz`;
		}
		return f;
	}

	getProject() {
		return this.project;
	}

	createEntry(url: string, ref?: string, maxDepth?: number, isResource?: boolean) {
		if (this.index.has(url)) throw new Error(`url index ${url} is already set`);
		let e = this.graph.createNode();
		e.setAttribute(URL, url);
		if (ref) e.setAttribute(FIRST_REF, ref);
		if (isResource) maxDepth = null;
		let allowed = maxDepth === 0 ? false: this.urlFilter(url, isResource);
		e.setAttribute(ALLOWED, allowed);
		if (maxDepth > 0) e.setAttribute(MAX_DEPTH, maxDepth);
		e.setAttribute(UPDATED_THIS_RUN, true);
		e.setAttribute(LAST_SEEN, this.project.getCurrentDownloadTime());
		this.index.set(url, e);
		debug('createEntry', url, ref, allowed);
		this.project.emit('newUrlEntry', e);
		return e;
	}

	updateEntry(e: Node, maxDepth?: number, isResource?: boolean) {
		if (e.getAttribute(UPDATED_THIS_RUN)) throw new Error('has already been updated: '+ e.getAttribute(URL));
		if (isResource) maxDepth = null;
		let allowed = maxDepth === 0 || this.project.noUpdates ? false : this.urlFilter(e.getAttribute(URL), isResource);
		e.setAttribute(ALLOWED, allowed);
		if (maxDepth > 0) e.setAttribute(MAX_DEPTH, maxDepth);
		e.setAttribute(UPDATED_THIS_RUN, true);
		e.setAttribute(LAST_SEEN, this.project.getCurrentDownloadTime());
	}

	getEntryFromUrl(url: string): Node|null {
		return this.index.get(url);
	}

	getEntryFromPath(path: string): Node|null {
		return this.index.get(path);
	}

	setPathForNode(node: Node, path: string) {
		if (node.getAttribute(PATH)) return;
		debug('set path for '+node.getAttribute(URL)+' - '+path);
		if (this.index.has(path) && this.index.get(path) !== node) {
			let ext = Path.extname(path);
			let base = Path.basename(path, ext);
			let dir = Path.dirname(path);
			let hasSuffix = base.match(/_\d+$/);
			let num: number;
			if (hasSuffix) {
				let suffixStart = base.lastIndexOf('_');
				num = parseInt(base.substr(suffixStart+1));
			} else {
				num = 1;
			}
			path = `${dir}/${base}_${num}${ext}`;
			debug('path changed to '+path);
		}
		node.setAttribute(PATH, path);
		this.index.set(path, node);
	}

	getUnusedFiles(): NodeCollection {
		// let current = this.project.getCurrentDownloadTime();
		// return this.graph.getCollection()
		// 	.filter((n: Node) =>
		// 		n.getAttribute(REMOVED) !== true
		// 		&& n.getAttribute(LAST_DOWNLOAD)
		// 		&& (n.getAttribute(LAST_DOWNLOAD) < current
		// 			&& n.getAttribute(LAST_SEEN) < current));
		let entry = this.getEntryFromUrl(this.project.httpEntry);
		let active = this.graph.getCollection(new Set());
		active.add(entry);
		active = active.gatherNodesWithinHops(99999, (e: Node) => {
			let links = e.getLinks((l) => l.getTo() === e && l.getAttribute(LINK_USED) === true);
			// console.log('node', e.getAttribute(URL), links.count());
			return e === entry || links.count() > 0;
		}, false, true);
		// console.log('active: ', active.count());
		let inactive = this.graph.filter(e => e.getAttribute(REMOVED) !== true);
		for (let node of active) {
			inactive.remove(node);
		}
		// for (let e of inactive) {
		// 	console.log('inactive', e.getAttribute(URL));
		// }
		return inactive;
	}

	getAll(): NodeCollection {
		return this.graph.getCollection();
	}

	shouldSkipExisting(node: Node): boolean {
		if (this.project.noUpdates) return true;
		if (!this.project.skipExistingFiles) return false;
		if (this.project.skipExistingFilesExclusion) {
			let mime = node.getAttribute(MIME);
			if (this.project.skipExistingFilesExclusion[mime]) return false;
		}
		return true;
	}
}
