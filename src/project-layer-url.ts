import ProjectBasis from "./project-basis";
import { URL } from 'url';
import Crypto from 'crypto';
import Path from 'path';
import Debug from 'debug';
const debug = Debug('tc-project-url');
import got, { Options, RequestError, Response } from 'got';
import { IncomingHttpHeaders } from "http";
import { Task } from "@stormking/datastructures";

export default class ProjectLayerUrl extends ProjectBasis {

	protected static httpTimeoutsDefault = {
		lookup: 2000,
		connect: 2000,
		secureConnect: 2000,
		socket: 2000,
		response: 3000,
		send: 5000,
		request: 10000
	};

	private fetchQueue = new Task();
	private hostAllowsHeaders = new Map<string,boolean>();

	fetchUrlHeaders(target: string, referer: string): Promise<IncomingHttpHeaders> {
		return this.fetchQueue.addTask(async() => {
			if (!this.noDelayOnHeaders) await this.delayFetch();
			return this.fetchUrlHeadersQueued(target, referer)
		});
	}

	async fetchUrl(target: string, referer: string): Promise<Response> {
		return this.fetchQueue.addTask(async() => {
			await this.delayFetch();
			return this.fetchUrlQueued(target, referer)
		});
	}

	async fetchUrlQueued(target: string, referer: string): Promise<Response> {
		let opts = this.getRequestOptions(referer);
		debug('fetchUrl', target);
		this.emit('httpFetch', { url: target, referer });
		let res: Response;
		try {
			res = <Response>await got(target, opts);
		} catch (e) {
			if (this.ignoreBadStatusCodes && e instanceof RequestError && e.message.includes('Response code') && e.response) {
				res = e.response;
			} else {
				throw e;
			}
		}
		return res;
	}

	async fetchUrlHeadersQueued(target: string, referer: string): Promise<IncomingHttpHeaders> {
		let opts = this.getRequestOptions(referer);
		let url = new URL(target);
		if (this.hostAllowsHeaders.get(url.hostname) !== false) {
			opts.method = 'HEAD';
		}
		opts.retry = {
			limit: 2,
			calculateDelay: () => {
				return 1;
			}
		};
		opts.hooks = {
			beforeRetry: [
				(opts, error, retryCount) => {
					if (retryCount === 1 && error.response.statusCode === 405) {
						this.hostAllowsHeaders.set(url.hostname, false);
						opts.method = 'GET'
					} else {
						throw error;
					}
				}
			]
		}
		//@TODO figure out way to ignore response body on GET rewrite
		this.emit('httpHeader', { url: target, referer });
		let res = <Response>await got(target, opts);
		debug('fetchHeaders', target, res.headers['content-type']);
		return res.headers;
	}

	private getRequestOptions(referer: string): Options {
		const opts: Options = {};
		opts.agent = {
			http: this.httpAgent,
			https: this.httpsAgent
		};
		opts.timeout = this.httpTimeouts;
		opts.retry = {
			limit: this.maxRetries,
			calculateDelay: ({ attemptCount, retryOptions, error, computedValue }) => {
				if (attemptCount > this.maxRetries || error.message.includes('404')) return 0;
				let baseDelay = this.getWaitTime();
				this.emit('fetchRetry', { attemptCount, error });
				return this.retryDelay({ baseDelay, attemptCount, retryOptions, error, computedValue, project: this });
			}
		};
		opts.http2 = true;
		opts.throwHttpErrors = true;
		let dynHeaders: any = {};
		dynHeaders['user-agent'] = this.userAgent;
		if (referer) dynHeaders.referer = referer;
		opts.headers = Object.assign({}, this.fetchHeaders, dynHeaders);
		opts.cookieJar = this.fetchCookies;
		return opts;
	}

	runMimeRules(url) {
		if (!this.mimeRules) return false;
		return this.mimeRules( url );
	}

	async lookupMime(url: string, fallback?: string, referer?: string): Promise<string> {
		let type: string;
		//from mime rules
		type = this.runMimeRules(url);

		//from headers
		if (!type && !this.skipFetchHeaders) {
			try {
				let headers = await this.fetchUrlHeaders(url, referer);
				let fromHeader = headers ? headers['content-type'] : null;
				if (fromHeader) {
					let cpos = fromHeader.indexOf(";");
					if (cpos > -1) {
						fromHeader = fromHeader.substring(0,cpos);
					}
				}
				if (fromHeader) {
					type = fromHeader;
				}
			} catch (e) {
				debug('error fetching mime from header', e);
			}
		}

		//from url expectation
		if (!type) {
			let parsed = new URL(url);
			type = this.mime.getType(parsed.pathname);
		}
		if (!type) {
			type = fallback ? fallback : this.defaultMimeType;
		}
		return type;
	}

	normalizeUrlDefault( url ): string {
		let parts: URL;
		if (typeof url === 'string') {
			parts = new URL(url);
		} else {
			parts = new URL(url.href);
		}
		//old aggressive option
		//parts.pathname = parts.pathname.replace(/([^ -~])|(%[0-9a-z]{2})/ig,'');
		if (parts.hash) {
			parts.hash = '';
		}
		// parts.host = this.mapDomain(parts.host);	//this includes port if specified
		if (parts.href.includes('page1')) debug('normalize', parts, parts.toString());
		return parts.toString().replace(/#$/, '');	//there is a bug where a # with no text in does not get removed
	}

	mapDomain(domain: string) {
		if (this.domainMapCache.has(domain)) {
			return this.domainMapCache.get(domain);
		}
		if (!this.domainMap || !this.domainMap.length) return domain;
		for (let i=0, ii=this.domainMap.length; i<ii; i++) {
			let [testRegex,mapping] = this.domainMap[i];
			if (domain.match(testRegex)) {
				this.domainMapCache.set(domain,mapping);
				return mapping;
			}
		}
		this.domainMapCache.set(domain,domain);
		return domain;
	}

	delayFetch() {
		return new Promise(res => setTimeout(res, this.getWaitTime()));
	}

	getWaitTime(): number {
		if (!this.randWaitTime) {
			return this.baseWaitTime;
		}
		return this.baseWaitTime + Math.random() * this.randWaitTime;
	}

	retryDelayDefault({ baseDelay, attemptCount, retryOptions, error, computedValue, project }) {
		return baseDelay + attemptCount * 1000;
	}

}
