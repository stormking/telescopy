import StaticServer from 'static-server';
import Project from '../../src/project';
import { expect } from 'chai';
import { addTests } from './server1tests';
import { exec } from 'child_process';
import { getEventCounts } from '../util/tests';

describe('Telescopy Stopping and Resuming', () => {
	let server;
	let project: Project;
	let counter;
	const local = '/tmp/tcopy-test/';
	before((done) => {
		server = new StaticServer({
			rootPath: `${__dirname}/../fixtures/server1/`,
			port: 8080,
			followSymlink: true
		});

		server.start(done);
	});

	after(() => {
		server.stop();
	});

	async function createProject(clean: boolean) {
		project = new Project({
			remote: 'http://localhost:8080/index.html',
			local,
			cleanLocal: clean,
			storage: 'file',
			noDbCompress: true,
			skipExistingFiles: true,
			skipExistingFilesExclusion: {
				'text/html': true
			},
			mimeRules: [{
				type: 'path',
				test: '/\\.php$/',
				match: 'text/html'
			}]
		});
		counter = getEventCounts(project);
	}

	async function runFor(n) {
		let count = 0;
		let cb = () => {
			count += 1;
			// console.log(`stop counter ${count} / ${n}`);
			if (count === n) {
				project.stop();
				project.off('httpFetch', cb);
			}
		}
		project.on('httpFetch', cb);
		await project.start();
		await project.endPromise;
		// console.log(counter());
		expect(count).to.equal(n);
	}

	function getFileList(): Promise<string[]> {
		return new Promise((res, rej) => {
			exec(`find ${local} -type f | sort`, (err, stdout) => {
				if (err) rej(err);
				else res(stdout.split('\n').filter(e => !!e));
			})
		});
	}

	describe('first run', () => {

		it('can stop after 6 files', async() => {
			createProject(true);
			await runFor(6);
		});

		it('has written 7 files', async() => {
			let files = await getFileList();
			// console.log(files);
			expect(files.length).to.equal(7);
		})

	});

	describe('second run', () => {

		it('can resume for another 6 files', async() => {
			createProject(false);
			await runFor(6);
		});

		it('has written 10 files', async() => {
			let files = await getFileList();
			// console.log(files);
			expect(files.length).to.equal(10);
		})

	});

	describe('third run', () => {

		it('can run the rest', async() => {
			createProject(false);
			await project.start();
			await project.endPromise;
			// console.log(counter());
		});

		describe('full result tests',() => {
			let setProject = addTests();

			before(() => {
				setProject(project)
			})
			it('has written 20 files ', async() => {
				let files = await getFileList();
				// console.log(files);
				expect(files.length).to.equal(20);
			})
		});

	});



});
