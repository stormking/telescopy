import Resource from "./resource";
import cheerio from 'cheerio';
import { Rule } from "./html-filters";

export default async function transformHtml(resource: Resource, buffer: string) {
	const $ = cheerio.load(buffer);
	let userHook = resource.getProject().domHook || function(){ return Promise.resolve() };
	if (resource.getProject().domHookBeforeProcessing) await userHook.apply(resource, [$, resource]);
	await processResource($, resource);
	if (!resource.getProject().domHookBeforeProcessing) await userHook.apply(resource, [$, resource]);
	return $.html();
}

async function processResource(dom: cheerio.Root, resource: Resource) {
	const project = resource.getProject();
	let wait = [];
	project.tagCallbacks.forEach((instruction: Rule) => {
		let elems = dom(instruction.selector);
		elems.each(function() {
			wait.push((async(elem) => {
				if (instruction.filter && !instruction.filter(elem)) return;

				switch (instruction.action) {
					default:
					case 'process':
						let value = elem.attr(instruction.attribute);
						let fbmime;
						if (instruction.mime.call) {
							let e = <any> elem['0'];
							fbmime = instruction.mime(e.attribs,resource);
						} else {
							fbmime = ""+instruction.mime;
						}
						let isResource = !!instruction.resource;
						value = await resource.processResourceLink(value, fbmime, isResource);
						if (!value) {
							elem.remove();
						} else {
							elem.attr(instruction.attribute, value);
						}
					break;

					case 'remove':
						elem.remove();
					break;

					case 'exec':
						await instruction.exec(elem, resource);
					break;
				}
			})(dom(this)));

		})

	});
	await Promise.all(wait);
}
