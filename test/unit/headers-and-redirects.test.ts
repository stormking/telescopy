import { expect } from 'chai';
import * as http from 'http';
import Project from '../../src/project';
import ProjectSettings from '../../src/project-settings';

describe('Headers', () => {

	let server;
	let onRequest;
	before((done) => {
		server = http.createServer((req, res) => {
			// console.log(req.method, req.url);
			if (onRequest) onRequest(req, res);
			else res.end(500);
		});
		server.listen(8080, done);
	});
	after(() => {
		server.close();
	})

	describe('simple fetch headers', () => {
		before(async() => {
			onRequest = (req,res) => {
				res.writeHead(200,{
					'Content-Type': 'text/html'
				});
				res.end('<p>hello</p>');
			}
		});
		it('can fetch headers', async() => {
			let s: ProjectSettings = {
				local: '.',
				storage: 'memory',
				remote: 'http://localhost:8080'
			};
			let p = new Project(s);
			let res = await p.fetchUrlHeaders('http://localhost:8080/a', '');
			// console.log(res);
			expect(res['content-type']).to.equal('text/html');
		});
	})

	describe('fetch with redirect', () => {
		before(async() => {
			onRequest = (req,res) => {
				if (req.url === '/page') {
					res.writeHead(307,{
						'Location': '/page1'
					});
					res.end();
				} else if (req.url === '/page1') {
					res.writeHead(200,{
						'Content-Type': 'text/html'
					});
					res.end('<p>hello</p>');
				} else {
					res.writeHead(404);
					res.end();
				}
			}
		});
		it('can fetch headers', async() => {
			let s: ProjectSettings = {
				local: '.',
				storage: 'memory',
				remote: 'http://localhost:8080'
			};
			let p = new Project(s);
			let res = await p.fetchUrlHeaders('http://localhost:8080/page', '');
			// console.log(res);
			expect(res['content-type']).to.equal('text/html');
		});
	});

	describe('fetch with only get allowed', () => {
		before(async() => {
			onRequest = (req,res) => {
				if (req.method === 'GET') {
					res.writeHead(200,{
						'Content-Type': 'text/html'
					});
					res.end('<p>hello</p>');
				} else {
					res.writeHead(405);
					res.end();
				}
			}
		});
		it('can fetch headers', async() => {
			let s: ProjectSettings = {
				local: '.',
				storage: 'memory',
				remote: 'http://localhost:8080',
				maxRetries: 2,
				retryDelay() {
					return 1;
				}
			};
			let p = new Project(s);
			let res = await p.fetchUrlHeaders('http://localhost:8080/page', '');
			expect(res['content-type']).to.equal('text/html');
		});
	});


});
