import Project from './project';
import parser from 'yargs-parser';
import * as Path from 'path';
import * as FS from 'fs';
import ProjectSettings from './project-settings';
import Chalk from 'chalk';
import pad from 'pad';
import ProjectTest from './project-test';
import { Node, Queue } from '@stormking/datastructures';
import logUpdate from 'log-update';
import { ALLOWED, DONE, FAILED, LAST_DOWNLOAD, LAST_SEEN, MIME, QUEUED, REMOVED, SKIP, URL } from './project-state';

const argv = parser(process.argv);
if (!argv.f && !argv.r || argv.h) {
	console.log('options: -r <remote-url> -f <config-file> --test --summary -d <number>');
	process.exit(0);
}
let project;
process.on('unhandledRejection', gracefulShutdown);
process.on('uncaughtException', gracefulShutdown);
process.on('SIGTERM', shutdown);
process.on('SIGINT', shutdown);

async function run() {
	const config = await getConfig(argv);
	console.log(config);
	if (argv.summary) {
		await showSummary(config);
	} else if (argv.test) {
		await runTest(config);
	} else {
		await runProject(config);
	}
}

run();


async function showSummary(config) {
	project = new ProjectTest(config);
	await project.getStorage().prepare({
		clean: false,
		path: project.localPath
	});
	let state = project.getState();
	await state.init();
	let nodes = state.getAll();
	let index = new Map();
	function add(key, val) {
		let values: Map<any, any>;
		if (index.has(key)) {
			values = index.get(key);
		} else {
			values = new Map();
			index.set(key, values);
		}

		// let store = `${key}-${val}`;
		if (!values.has(val)) {
			values.set(val, 1);
		} else {
			values.set(val, values.get(val) + 1);
		}
	}
	let keys = [ALLOWED, DONE, FAILED, MIME, QUEUED, REMOVED, LAST_DOWNLOAD, LAST_SEEN, SKIP];
	for (let node of nodes) {
		let map = node.getAttributesClone();
		keys.forEach(key => {
			let val = map.has(key) ? map.get(key) : false;
			add(key, val);
		})
	}
	console.log(index);
}

async function runTest(config: ProjectSettings) {
	config.cleanLocal = true;
	config.storage = 'memory';
	config.local = '/tmp/telescopy';
	config.baseWaitTime = 0;
	config.randWaitTime = 0;
	config.skipFetchHeaders = true;
	project = new ProjectTest(config);
	await project.start();
	await project.endPromise;
	let res = project.getResults();
	printTestResult(res);
}

async function runProject(config: ProjectSettings) {
	const latestNodes = new Queue<Node>();
	let latestHeader = '';
	let retries = 0;
	project = new Project(config);
	project.on('resourceStart', (node: Node) => {
		retries = 0;
		latestHeader = '';
		if (node.getAttribute(SKIP)) return;
		latestNodes.push(node);
		if (latestNodes.getSize() > 20) latestNodes.shift();
		showProgressSummary(project, latestNodes, latestHeader, retries);
	});
	project.on('httpHeader', ({ url }) => {
		latestHeader = url;
		retries = 0;
		showProgressSummary(project, latestNodes, latestHeader, retries);
	});
	project.on('fetchRetry', ({ attemptCount }) => {
		retries = attemptCount;
		showProgressSummary(project, latestNodes, latestHeader, retries);
	})
	await project.start();
	await project.endPromise;
	showProgressSummary(project, latestNodes, latestHeader, retries);
}
function gracefulShutdown(err) {
	console.log('unhandled exception caught, shutting down...', err);
	if (project) {	//this doesnt work. must be sync
		project.stop();
	}
}
let shuttingDown = false;
function shutdown(){
	if (shuttingDown) {
		console.log('killing process forcefully');
		process.exit(5);
	}
	console.log("shutting down, please wait...");
	shuttingDown = true;
	if (project) {
		project.stop();
	}
};


function showProgressSummary(project: Project, latestNodes: Queue<Node>, latestHeader: string, retries: number) {
	let collection = project.getState().getAll();
	let stats = {
		all: 0,
		queued: 0,
		done: 0,
		failed: 0,
		allowed: 0,
		denied: 0
	};
	let urls = [];
	for (let node of collection) {
		stats.all += 1;
		if (node.getAttribute(ALLOWED)) {
			stats.allowed += 1;
		} else {
			stats.denied += 1;
		}
		if (node.getAttribute(DONE)) {
			if (node.getAttribute(FAILED)) {
				stats.failed += 1;
			} else {
				stats.done += 1;
			}
		} else if (node.getAttribute(ALLOWED) && !node.getAttribute(SKIP)) {
			stats.queued += 1;
		}
	}
	for (let node of latestNodes) {
		let status = !node.getAttribute(DONE) ? ' '
			: node.getAttribute(FAILED) ? Chalk.red('✗') : Chalk.blue('✓');
		let u = `${status} ${node.getAttribute(URL)}`;
		urls.push(u);
	}
	let visRetries = Chalk.red('↺'.repeat(retries));
	logUpdate(`
${urls.join("\n")}
  ${Chalk.gray(latestHeader)} ${visRetries}

URLs: ${stats.all} total
Status: ${stats.queued} queued / ${stats.done} done / ${stats.failed} failed
Filter: ${stats.allowed} allowed / ${stats.denied} denied
	`);
}

async function getConfig(argv) {
	const configPath = argv.f || argv.file;
	let config = null;
	if (configPath) {
		config = await loadOptions(configPath);
	} else {
		config = {};
	}
	let remote = argv.remote || argv.url || argv.u || argv.r;
	if (remote) {
		config.remote = remote;
	}
	let local = argv.l || argv.local;
	if (local) {
		config.local = local;
	}
	if (!config.local) {
		config.local = '/tmp/telescopy';
		config.storage = 'memory';
	}
	let depth = argv.d || argv.depth;
	if (!isNaN(depth)) {
		config.maxCrawlDepth = ~~depth;
	}
	return config;
}

function printTestResult(nodes: Set<Node>) {
	let tl1 = 0;
	let tl3 = 0;
	let res = Array.from(nodes);
	res = res.sort((a,b) => {
		return a.getAttribute(URL) > b.getAttribute(URL) ? 1 : -1;
	});
	res.forEach(a => {
		tl1 = Math.max(tl1, a.getAttribute(URL).length);
		tl3 = Math.max(tl3, a.getAttribute(MIME) ? a.getAttribute(MIME).length : 0);
	});
	tl1 += 2;
	tl3 += 2;
	res.forEach(r => {
		let c;
		let str = '';
		if (r.getAttribute(ALLOWED)) c = Chalk.bgGreen.black;
		else c = Chalk.bgYellow.black;
		str += pad( (r.getAttribute(URL)), tl1, { colors: true} );
		// str += pad( 5, ''+i, { colors: true} );
		str += pad( tl3, (r.getAttribute(MIME) ? r.getAttribute(MIME) : ""), { colors: true} );
		console.log(c(str));
	});
}

async function loadOptions(fileName: string): Promise<ProjectSettings> {
	const ext = Path.extname(fileName);
	let options;
	switch (ext) {
		case '.js':
			//make relative paths absolute for require
			if (['.','/'].includes(fileName[0])) {
				fileName = Path.join(process.cwd(),fileName);
			}
			options = require(fileName);
			if (typeof options === 'function') {
				options = await options();
			}
		break;

		case '.json':
			options = JSON.parse( FS.readFileSync( fileName, { encoding: 'utf8' } ) );
		break;

		default:
			throw new Error("invalid options file given, must be .js or .json");
	}
	if (!(options instanceof Object)) {
		throw new Error("did not get options-object from "+fileName);
	}
	return options;
}
