import StaticServer from 'static-server';
import Project from '../../src/project';
import { expect } from 'chai';
import * as cheerio from 'cheerio';
import filter from '../../src/filter';
import { URL } from 'url';
import { postProcessImageToMaxSize } from '../../src/image-magick';

describe('Telescopy Post Processing', () => {
	let server;
	let project;
	const local = '/tmp/tcopy-test/';
	async function getCheerio(f) {
		let buf = await project.getStorage().retrieve(f);
		const $ = cheerio.load(buf);
		return $;
	}
	before((done) => {
		server = new StaticServer({
			rootPath: `${__dirname}/../fixtures/server1/`,
			port: 8080,
			followSymlink: true
		});
		server.start(done);
	});

	after(() => {
		server.stop();
	});

	describe('filter out paths', () => {
		it('can run the project', async() => {
			project = new Project({
				remote: 'http://localhost:8080/index.html',
				local,
				cleanLocal: true,
				storage: 'memory',
				postProcessingHooks: [{
					urlFilter: [{
						type: 'path',
						match: true,
						test: '/\\.css$/'
					}, false],
					hook: (input, url, mime) => {
						if (url.includes('app.css')) {
							let content = input.toString('utf8');
							content = content.replace('#fff', 'red');
							return content;
						}
						return null;
					}
				}, {
					mime: 'text/html',
					hook: (input, url) => {
						if (url.includes('contact.html')) {
							let content = input.toString('utf8');
							content = content.replace('<title>index</title>', '<title>contact</title>');
							return content;
						}
						return null;
					}
				}]
			});
			await project.start();
			await project.endPromise;
		});

		it('has changed app.css', async() => {
			let $ = await getCheerio('localhost/contact.html');
			let title = $('title');
			expect(title.length).to.equal(1);
			expect(title.text()).to.equal('contact');
		});

		it('has changed contact.html', async() => {
			let buf = await project.getStorage().retrieve('localhost/media/app.css');
			let content = buf.toString('utf8');
			expect(content.includes('color: red')).to.be.true;
		});
	});

	describe('resize images', () => {
		it('can run the project', async() => {
			project = new Project({
				remote: 'http://localhost:8080/index.html',
				local,
				cleanLocal: true,
				storage: 'file',
				postProcessingHooks: [postProcessImageToMaxSize(50)]
			});
			await project.start();
			await project.endPromise;
		});

		it('has reduced size on image', async() => {
			let buf = await project.getStorage().retrieve(('localhost/media/icon1.png'));
			expect(buf.length).to.be.lessThan(4000);
		});
	});

});
