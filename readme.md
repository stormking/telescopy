# Telescopy v2.0-beta1 #

For Telescopy v1 check [v1 branch](https://gitlab.com/stormking/telescopy/-/tree/v1)

## v2 Beta Status

v2 has been tested on a few real websites, and most features are well covered by unit and integration tests.

### Major Changes

- extensible storage layer
- stores the project state as json-db
- settings are mostly compatible with v1
- easy to add host block-list to surpress ads
- remove external resources entirely
- better cli
- full typescript support
- post-processing options

### Full Docs

TBD

### Usage

`node bin/run.js -f ./myconfig.js` - runs the project as specified by the myconfig.js file

- `-f ./file.js(on)` - specify .js or .json config. .js should be node-requireable file with module.exports = config
- `-r http://example.com` - specifies are different entry url for starting the project than what is defined in the config
- `-d 3` - limit the depth of links downloaded to 3
- `--test` - runs a test, showing what URLs are allowed to be downloaded on the given entry page
- `--summary` - shows the project summary as it is known to the state file
