import { ChildProcess, spawn } from "child_process";
import { Readable } from "stream";
import { PostProcessing } from "./project-settings";

function pipeTo(cmd, args, readable): ChildProcess {
	let sub = spawn(cmd, args, { });
	// process.stdout.on('data', s => console.log(s));
	// process.stderr.on('data', s => console.log(s));
	readable.pipe(sub.stdin);
	return sub;
}

function getOutput(sub: ChildProcess): Promise<Buffer> {
	return new Promise((resolve, reject) => {
		let buf: Buffer[] = [], err: Buffer[] = [];
		sub.stdout.on('data',b => buf.push(b));
		sub.stderr.on('data', b => err.push(b));
		sub.on('close', (code: number) => {
			if (code > 0) {
				reject(new Error(`spawn process failed: ${code} - message: ${Buffer.concat(err).toString('utf8')}`))
			} else {
				resolve(Buffer.concat(buf));
			}
		});
	})
}

function createInputPipe(buf: Buffer): Readable {
	let pointer = 0;
	return new Readable({
		read(size) {
			if (pointer < buf.length) {
				let len = Math.max(size, buf.length - pointer);
				this.push(buf.slice(pointer, len));
				pointer += len;
			} else {
				this.push(null);
			}
		}
	});
}

async function getImageSize(input) {
	let outBuf = await getOutput(pipeTo('identify', ['-'], createInputPipe(input)));
	let out = outBuf.toString('utf8');
	let match = out.match(/^[^\s]+ ([A-Z]+) ([0-9]+)x([0-9]+)/);
	if (!match) throw new Error('unable to find size: '+out);
	return {
		type: match[1].toLowerCase(),
		width: ~~match[2],
		height: ~~match[3]
	};
}


export function postProcessImageToMaxSize(maxWidth: number, maxHeight?: number): PostProcessing {
	return {
		urlFilter: [{
			type: 'path',
			match: true,
			test: '/\\.(jpg|jpeg|png)/'
		}, false],
		async hook(input: Buffer, url: string, mime: string) {
			if (!maxHeight) maxHeight = maxWidth;
			try {
				//need info not just for size but also type in case mime is unreliable
				let info = await getImageSize(input);
				if (info.width > maxWidth || info.height > maxHeight) {
					let out = await getOutput(pipeTo('convert',
						[`${info.type}:-` , `-resize`, `${maxWidth}x${maxHeight}`, `${info.type}:-`],
						createInputPipe(input)));
					return out;
				}
			} catch (e) {
				console.log(e);
			}
			return null;
		}
	}
}
