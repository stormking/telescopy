import StaticServer from 'static-server';
import Project from '../../src/project';
import { addTests } from './server1tests';

describe('Telescopy', () => {
	let server;
	let project: Project;
	const local = '/tmp/tcopy-test/';
	before((done) => {
		server = new StaticServer({
			rootPath: `${__dirname}/../fixtures/server1/`,
			port: 8080,
			followSymlink: true
		});
		server.start(done);
	});

	after(() => {
		server.stop();
	});

	it('can run the project', async() => {
		project = new Project({
			remote: 'http://localhost:8080/index.html',
			local,
			cleanLocal: true,
			storage: 'file',
			mimeRules: [{
				type: 'path',
				test: '/\\.php$/',
				match: 'text/html'
			}]
		});
		await project.start();
		await project.endPromise;
	});

	describe('result tests',() => {
		let setProject = addTests();
		before(() => {
			setProject(project)
		})
	});

});
