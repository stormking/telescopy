import StaticServer from 'static-server';
import Project from '../../src/project';
import { expect } from 'chai';
import * as cheerio from 'cheerio';
import * as FS from 'fs';
import { getEventCounts } from '../util/tests';

describe('Telescopy Re-Mirroring', () => {
	let server;
	let project;
	const local = '/tmp/tcopy-test/';
	async function getCheerio(f) {
		let buf = await project.getStorage().retrieve(f);
		const $ = cheerio.load(buf);
		return $;
	}

	describe('first mirror', () => {
		let counter;
		before((done) => {
			server = new StaticServer({
				rootPath: `${__dirname}/../fixtures/server2a/`,
				port: 8080,
				followSymlink: true
			});
			server.start(done);
		});

		after(() => {
			// console.log(counter());
			server.stop();
			project = null;
			server = null;
		});

		it('can run the project', async() => {
			project = new Project({
				remote: 'http://localhost:8080/index.html',
				local,
				cleanLocal: true,
				storage: 'file',
				noDbCompress: true
			});
			counter = getEventCounts(project);
			await project.start();
			await project.endPromise;
		});

		it('has written the database', async() => {
			let exists;
			try {
				let db = await FS.promises.stat(`${local}/project.jsonnd`);
				exists = true;
			} catch (e) {
				exists = false;
			}
			expect(exists).to.be.true;
		});

		it('has written index, about, page1', async() => {
			let has = await project.getStorage().has(('localhost/index.html'));
			expect(has).to.be.true;
			has = await project.getStorage().has(('localhost/page1.html'));
			expect(has).to.be.true;
			has = await project.getStorage().has(('localhost/about.html'));
			expect(has).to.be.true;
		});

		it('has fetched 3 urls', () => {
			expect(counter().resourceStart).to.equal(3);
		})

	});

	describe('second mirror', () => {
		let counter;
		before((done) => {
			server = new StaticServer({
				rootPath: `${__dirname}/../fixtures/server2b/`,
				port: 8080,
				followSymlink: true
			});
			server.start(done);
		});

		after(() => {
			// console.log(counter());
			server.stop();
			project = null;
			server = null;
		});

		it('can run the project', async() => {
			project = new Project({
				remote: 'http://localhost:8080/index.html',
				local,
				cleanLocal: false,
				cleanUnused: true,
				storage: 'file',
				skipExistingFiles: true,
				skipExistingFilesExclusion: {
					'text/html': true
				},
				noDbCompress: true
			});
			counter = getEventCounts(project);
			await project.start();
			await project.endPromise;
		});

		it('has added page2', async() => {
			let has = await project.getStorage().has(('localhost/page2.html'));
			expect(has).to.be.true;
		});

		it('has added stylesheet', async() => {
			let has = await project.getStorage().has(('localhost/stylesheet.css'));
			expect(has).to.be.true;
		});

		it('has added bg2.jpg', async() => {
			let has = await project.getStorage().has(('localhost/bg2.jpeg'));
			expect(has).to.be.true;
		});

		it('has updated index', async() => {
			let $ = await getCheerio('localhost/index.html');
			let link = $('#l1');
			expect(link.length).to.equal(1);
			expect(link.attr('href')).to.equal('page2.html');
		});

		it('has removed about', async() => {
			let has = await project.getStorage().has(('localhost/about.html'));
			expect(has).to.be.false;
		});

		it('has not changed page1', async() => {
			let has = await project.getStorage().has(('localhost/page1.html'));
			expect(has).to.be.true;
		});

		it('has fetched 5 urls', () => {
			expect(counter().httpFetch).to.equal(5);
		})

	});

	describe('third mirror', () => {
		let counter;
		before((done) => {
			server = new StaticServer({
				rootPath: `${__dirname}/../fixtures/server2c/`,
				port: 8080,
				followSymlink: true
			});
			server.start(done);
		});

		after(() => {
			// console.log(counter());
			server.stop();
			project = null;
			server = null;
		});

		it('can run the project', async() => {
			project = new Project({
				remote: 'http://localhost:8080/index.html',
				local,
				cleanLocal: false,
				storage: 'file',
				skipExistingFiles: true,
				skipExistingFilesExclusion: {
					"text/html": true
				},
				retryDelay(a) {
					throw new Error('stop re-attempt');
				},
				noDbCompress: true
			});
			counter = getEventCounts(project);
			await project.start();
			await project.endPromise;
		});

		it('has added page2', async() => {
			let has = await project.getStorage().has(('localhost/page2.html'));
			expect(has).to.be.true;
		});

		it('has updated index', async() => {
			let $ = await getCheerio('localhost/index.html');
			let link = $('#l1');
			expect(link.length).to.equal(1);
			expect(link.attr('href')).to.equal('page2.html');
		});

		it('has removed about', async() => {
			let has = await project.getStorage().has(('localhost/about.html'));
			expect(has).to.be.false;
		});

		it('has not changed page1', async() => {
			let has = await project.getStorage().has(('localhost/page1.html'));
			expect(has).to.be.true;
		});

		it('has not updated stylesheet', async() => {
			let style = await project.getStorage().retrieve(('localhost/stylesheet.css'));
			expect(style.toString('utf8')).to.include('blue');
		});

		it('has kept bg2.jpg', async() => {
			let has = await project.getStorage().has(('localhost/bg2.jpeg'));
			expect(has).to.be.true;
		});

		it('has not fetched any headers', () => {
			expect(counter().httpHeader).to.equal(0);
		})

	});

	describe('fourth mirror', () => {
		let counter;
		before((done) => {
			server = new StaticServer({
				rootPath: `${__dirname}/../fixtures/server2c/`,
				port: 8080,
				followSymlink: true
			});
			server.start(done);
		});

		after(() => {
			// console.log(counter());
			server.stop();
			project = null;
			server = null;
		});

		it('can run the project', async() => {
			project = new Project({
				remote: 'http://localhost:8080/index.html',
				local,
				cleanLocal: false,
				storage: 'file',
				noDbCompress: true,
				maxCrawlDepth: 2
			});
			counter = getEventCounts(project);
			await project.start();
			await project.endPromise;
		});

		it('has not reached page1', () => {
			expect(counter().resourceStart).to.equal(4);
		});
	});


});
