import ProjectSettings, { PostProcessing } from './project-settings';
import Path from 'path';
import URL from 'url';
import Mime from 'mime';
import ProjectState, { ALLOWED, DONE, MAX_DEPTH, REMOVED, MIME, PATH, URL as RES_URL, UPDATED_THIS_RUN, FAILED, SKIP, QUEUED, LINK_USED } from './project-state';
import HTTP from 'http';
import HTTPS from 'https';
import Resource from './resource';
import { CookieJar, Cookie } from 'tough-cookie';
import filter from './filter';
import ProxyAgent from 'proxy-agent';
import HtmlDomFilters from './html-filters';
import Debug from 'debug';
import { FileStorage, MemoryStorage, Storage } from './storage';
import { Queue, Node } from '@stormking/datastructures';
import ProjectLayerUrl from './project-layer-url';
const debug = Debug('tc-project');

export default class Project extends ProjectLayerUrl {

	constructor(settings: ProjectSettings) {
		super();
		this.initSettings(settings);
	}

	private initSettings(settings: ProjectSettings) {
		if (!settings.local) throw new Error('must specify option: local');
		if (!settings.remote) throw new Error('must specify option: remote');
		this.currentDownloadTime = Date.now();
		this.localPath = Path.normalize(settings.local);
		this.httpEntry = settings.remote;
		this.cleanLocal = settings.cleanLocal || false;
		this.cleanUnused = settings.cleanUnused || false;
		this.addIndex = 'addIndex' in settings ? settings.addIndex : true;
		this.noDbCompress = 'noDbCompress' in settings ? settings.noDbCompress : false;
		this.maxCrawlDepth = settings.maxCrawlDepth || 0;
		this.noUpdates = settings.noUpdates || false;
		//temp dir, optional
		this.tempDir = settings.tempDir || this.localPath+'/tmp/';
		//skip url if calculated local path exists
		this.skipExistingFiles = !!settings.skipExistingFiles;
		//exclude some mime types from being skipped if they exist
		this.skipExistingFilesExclusion = settings.skipExistingFilesExclusion || { 'text/html': true };
		this.skipFetchHeaders = settings.skipFetchHeaders || false;
		//remove tags linking to resources that have been filtered out (anything not text/html)
		this.removeFilteredResource = settings.removeFilteredResource || false;
		//number of retries after timeouts
		this.maxRetries = settings.maxRetries || 3;
		//timeout to retrieving http headers
		this.timeoutToHeaders = settings.timeoutToHeaders || 6000;
		//timeout to full download completion
		this.timeoutToDownload = settings.timeoutToDownload || 12000;
		//create symlinks for http redirects
		this.linkRedirects = settings.linkRedirects || true;
		//expected index filename, e.g. is url ends with /
		this.defaultIndex = settings.defaultIndex || 'index';
		//default useragent
		this.userAgent = settings.userAgent || 'Telescopy website mirror';
		this.ignoreBadStatusCodes = settings.ignoreBadStatusCodes || false;
		//socks proxy url:port
		this.proxy = settings.proxy || null;
		//headers and cookies
		this.fetchHeaders = settings.fetchHeaders || {};
		this.fetchCookies = new CookieJar();
		if (settings.fetchCookies) {
			if (Array.isArray(settings.fetchCookies)) {
				settings.fetchCookies.forEach(str => {
					this.fetchCookies.setCookie(Cookie.parse(str), this.httpEntry);
				});
			}
		}
		//enable alternate transform html method
		this.domHook = settings.domHook || null;
		this.domHookBeforeProcessing = false;

		//build htrml attribute filters
		this.tagCallbacks = HtmlDomFilters(settings.htmlDomFilters);

		//init own mime container since it's very important for file naming
		this.mime = Mime;
		this.defaultMimeType = 'bin';	//@TODO able to change via config
		this.mime.define({
			'text/xml' : ['xml'],
			"text/html":["html","htm","php","php5","php3","php7","asp"]
		},true);
		if (settings.mimeDefinitions) {
			this.mime.define( settings.mimeDefinitions, true );
		}
		//mime custom rules
		if (settings.mimeRules) {
			this.mimeRules = filter( settings.mimeRules );
		}
		//url sanitation
		this.normalizeUrl = settings.normalizeUrl || this.normalizeUrlDefault;
		//header decision
		// this.decideOnHeaders = settings.decideOnHeaders || ProjectUtil.decideOnHeaders;

		this.domainMap = settings.domainMap || null;
		this.domainMapCache = new Map();

		//wait time between resources: base + random
	    this.baseWaitTime = settings.baseWaitTime || 0;
	    this.randWaitTime = settings.randWaitTime || 0;
		this.noDelayOnHeaders = settings.noDelayOnHeaders || false;

		if (settings.filterByUrl) {	//override function
			this.urlFilter = settings.filterByUrl;
		} else if (settings.urlFilter) {	//config
			this.urlFilter = filter(settings.urlFilter);
		} else {	//fallback to host filter
			let entryHost = new URL.URL(this.httpEntry).hostname;
			this.urlFilter = function(url: string, isResource: boolean) {
				// debug('urlFilter default', URL.parse(url).hostname, entryHost);
				if (isResource) return true;
				return new URL.URL(url).hostname === entryHost;
			}
		}
		if (settings.mimeRules) {
			this.mimeRules = filter(settings.mimeRules);
		}
		if (settings.postProcessingHooks) {
			this.postProcessingHooks = settings.postProcessingHooks.map((row: PostProcessing) => {
				if (row.urlFilter && typeof row.urlFilter !== 'function') {
					row.urlFilter = filter(row.urlFilter);
				}
				return row;
			});
		} else {
			this.postProcessingHooks = [];
		}

		let storageType = settings.storage || 'file';
		switch (storageType) {
			case 'file': {
				this.storage = new FileStorage();
				break;
			}
			case 'memory': {
				this.storage = new MemoryStorage();
				break;
			}
			default: {
				this.storage = storageType;
			}
		}

		//initialize project state
		this.state = new ProjectState( this );

		//init http or proxy agent
		this.agentsettings = {
			keepAlive : true,
			keepAliveMsecs : settings.agentKeepAlive || 3000,
			maxSockets : 1000,
			maxFreeSockets : 256
		};
		if (this.proxy) {
			this.httpAgent = this.httpsAgent = new ProxyAgent(this.proxy);
			this.httpAgent.keepAlive = false;
		} else {
			this.httpAgent = new HTTP.Agent(this.agentsettings);
			this.httpsAgent = new HTTPS.Agent(this.agentsettings);
		}
		this.retryDelay = settings.retryDelay || this.retryDelayDefault;
		this.httpTimeouts = Object.assign({}, Project.httpTimeoutsDefault, settings.httpTimeouts);

		this.queue = new Queue<Node>();

		this.endPromise = new Promise((res, rej) => {
			this.endResolve = res;
			this.endReject = rej;
		});
	}

	public async start() {
		if (this.running) {
			throw new Error("already running");
		}
		this.running = true;
		await this.prepareToRun();
		this.processNext();
	}

	public async stop() {
		this.running = false;
	}

	protected async processNext() {
		if (!this.running) {
			await this.state.save();
			this.endResolve();
			return;
		}
		try {
			let node: Node = this.queue.shift();
			debug('processNext: ', node ? node.getAttribute(RES_URL) : 'cleanup');
			if (!node) {
				await this.removeUnusedFiles();
				await this.state.save();
				this.endResolve();
				return;
			}
			this.emit('resourceStart', node);
			node.deleteAttribute(QUEUED);
			if (node.getAttribute(SKIP)) {
				debug('skip node');
			} else {
				let res = new Resource(this, node);
				try {
					await res.process();
					node.setAttribute(FAILED, false);
				} catch (e) {
					//this will often be a failure to fetch
					node.setAttribute(FAILED, true);
					this.emit('resourceError', e);
					// this.endReject(e);
					// return;
				}
			}
			node.setAttribute(DONE, true);
			for (let link of node.getLinks()) {
				let connected = link.getTo();
				if (connected.getAttribute(DONE) === true
					|| connected.hasAttribute(QUEUED)
					|| link.getAttribute(LINK_USED) === false
					|| connected.getAttribute(ALLOWED) !== true) {
					continue;
				}
				connected.setAttribute(QUEUED, true);
				if (connected.getAttribute(MIME) === 'text/html') {
					this.queue.push(connected);
				} else {
					this.queue.unshift(connected);
				}
			}
			this.emit('resourceDone', node);
			this.processNext();
		} catch (e) {
			this.endReject(e);
		}
	}

	protected async removeUnusedFiles() {
		if (!this.cleanLocal && !this.cleanUnused) return;
		let unused = this.state.getUnusedFiles();
		for (let node of unused) {
			let path = node.getAttribute(PATH);
			if (!path) continue;
			debug('removing file', path);
			await this.storage.remove(path);
			node.setAttribute(REMOVED, true);
			// node.destroy();
		}
	}

	protected async prepareToRun() {
		await this.getStorage().prepare({
			clean: this.cleanLocal,
			path: this.localPath
		});
		await this.state.init();
		if (this.httpEntry) {
			let entryNode = this.getState().getEntryFromUrl( this.httpEntry );
			if (!entryNode) {
				entryNode = this.getState().createEntry(this.httpEntry);
				entryNode.setAttribute(MIME, 'text/html');
			} else {
				entryNode.setAttribute(DONE, false);
				entryNode.setAttribute(UPDATED_THIS_RUN, true);
			}
			this.queue.push(entryNode);
			if (this.maxCrawlDepth) {
				entryNode.setAttribute(MAX_DEPTH, this.maxCrawlDepth);
			}
			await this.generateIndexFile(entryNode);
		}
		this.queueUpPendingNodes();
	}

	queueUpPendingNodes() {
		let nodes = this.getState().getAll().filter(
			(e: Node) => !e.getAttribute(DONE)
				&& e.getAttribute(ALLOWED)
				&& e.getAttribute(QUEUED)
				&& !e.getAttribute(SKIP)
		);
		for (let node of nodes) {
			debug('re-queue', node.getAttribute(RES_URL));
			if (node.getAttribute(MIME) === 'text/html') {
				this.queue.push(node);
			} else {
				this.queue.unshift(node);
			}
		}
	}

	getState(): ProjectState {
		return this.state;
	}

	getCurrentDownloadTime() {
		return this.currentDownloadTime;
	}

	getQueue(): Queue<Node> {
		return this.queue;
	}

	getPostProcessingHooks(url: string, mime: string): PostProcessing['hook'][] {
		return this.postProcessingHooks.filter((entry: PostProcessing) => {
			if (entry.mime && entry.mime !== mime) return false;
			if (entry.urlFilter && typeof entry.urlFilter === 'function' && !entry.urlFilter(url)) return false;
			return true;
		}).map(e => e.hook);
	}

	getStorage(): Storage {
		return this.storage;
	}

	async generateIndexFile(node: Node) {
		const indexPath = 'index.html';
		if (!this.addIndex || await this.storage.has(indexPath)) return;
		let res = new Resource(this, node);
		let rel = res.getLocalPath();
		let html = Buffer.from(`<!DOCTYPE html><html>
		<head><meta http-equiv="refresh" content="0;URL='${rel}'"/></head>
		<body><a href="${rel}">${rel}</a></body>
		</html>`);
		await this.storage.save(indexPath, html);
	}

	getNoDbCompress() {
		return this.noDbCompress;
	}

}
