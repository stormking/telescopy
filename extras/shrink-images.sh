inputfolder=$1
outfolder=/tmp/converted
maxsize=$2
mkdir $outfolder
find "$inputfolder" -type f -print0 | \
xargs -0 -I {} sh -c "f=\$(basename \"{}\"); d=\"$outfolder\$(dirname \"{}\")\"; mkdir -p \"\$d\"; echo \"NEXT: \$d/\$f\"; convert \"{}\" -resize ${maxsize}x${maxsize}\> \"\$d/\$f\""
