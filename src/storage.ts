import * as FS from 'fs';
import * as Path from 'path';
import rimraf from 'rimraf';
import mkdirp from 'mkdirp';
import { Readable, Writable } from 'stream';

export interface Storage {

	save(path: string, body: Buffer): Promise<void>

	has(path: string): Promise<boolean>

	retrieve(path: string): Promise<Buffer>

	prepare(settings: { clean: boolean, path: string }): Promise<void>

	remove(path: string): Promise<void>

	getWriteStream(path: string): Writable

	getReadStream(path: string): Readable

	[Symbol.iterator](): IterableIterator<string>
}

export class MemoryStorage implements Storage {

	protected files = new Map<string, Buffer>();

	async save(path: string, body: Buffer): Promise<void> {
		this.files.set(path, body);
	}

	async has(path: string): Promise<boolean> {
		return this.files.has(path);
	}

	async retrieve(path: string): Promise<Buffer> {
		if (!this.files.has(path)) throw new Error(`path does not exist: ${path}`);
		let r = this.files.get(path);
		return r;
	}

	async prepare() {}

	async remove(path: string): Promise<void> {
		this.files.delete(path);
	}

	getWriteStream(path: string): Writable {
		const tmp = [];
		const files = this.files;
		return new Writable({
			write(chk, enc, cb) {
				if (Buffer.isBuffer(chk)) {
					tmp.push(chk);
				} else {
					tmp.push(Buffer.from(chk, enc));
				}
				cb();
			},
			final(cb) {
				files.set(path, Buffer.concat(tmp));
			}
		})
	}

	getReadStream(path: string): Readable {
		let pointer = 0;
		const file = this.files.get(path);
		return new Readable({
			read(size) {
				let chk = file.slice(pointer, size);
				pointer += size;
				this.push(chk);
			}
		})
	}

	*[Symbol.iterator](): IterableIterator<string> {
		for (let path of Object.keys(this.files)) {
			yield path;
		}
	}

}

export class FileStorage implements Storage {

	protected pathsCreated = new Set<string>();
	protected basePath: string;

	async save(path: string, body: Buffer): Promise<void> {
		let full = Path.join(this.basePath, path);
		let dir = Path.dirname(full);
		if (!this.pathsCreated.has(dir)) {
			await mkdirp(dir);
			this.pathsCreated.add(dir);
		}
		return FS.promises.writeFile(full, body);
	}

	async has(path: string): Promise<boolean> {
		let full = Path.join(this.basePath, path);
		try {
			await FS.promises.stat(full);
			return true;
		} catch (e) {
			if (e.code === 'ENOENT') return false;
			throw e;
		}
	}

	retrieve(path: string): Promise<Buffer> {
		return FS.promises.readFile(Path.join(this.basePath, path));
	}

	async prepare({ clean, path }) {
		this.basePath = path;
		await mkdirp(Path.dirname(path));
		if (clean) {
			return new Promise<void>((resolve, reject) => {
		        rimraf(path, function(err){
		            if (err) reject(err);
		            else resolve();
		        });
			});
		}
	}

	async remove(path: string): Promise<void> {
		let full = Path.join(this.basePath, path);
		await FS.promises.unlink(full);
	}

	getWriteStream(path: string): Writable {
		let full = Path.join(this.basePath, path);
		return FS.createWriteStream(full);
	}

	getReadStream(path: string): Readable {
		let full = Path.join(this.basePath, path);
		return FS.createReadStream(full);
	}

	*[Symbol.iterator](): IterableIterator<string> {
		throw new Error('not implemented');
	}

}
