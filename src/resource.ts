import Project from './project';
import { Response } from 'got';
import Debug from 'debug';
import { Node } from '@stormking/datastructures';
import { URL } from 'url';
import * as Crypto from 'crypto';
import * as Path from 'path';
import transformCss from './transform-css';
import transformHtml from './transform-html';
import { MIME, URL as RES_URL, FIRST_REF, DONE, LAST_DOWNLOAD, MAX_DEPTH, LAST_SEEN, ALLOWED, PATH, BASE_URL, FAILED, UPDATED_THIS_RUN, LINK_USED } from './project-state';


export default class Resource {

	protected project: Project;
	protected referer: string;
	protected response: Response;
	protected node: Node;

	protected redirectUrl: string;
	protected baseUrl: string;

	protected _localPath: string;
	protected expectedLocalPath: string;


	constructor(project: Project, node: Node, referer?: string) {
		this.project = project;
		this.referer = referer;
		this.node = node;
	}

	protected debug(m, ...p) {
		Debug(`tc-resource-${this.getLinkedUrl()}`)(m, ...p);
	}

	getNode(): Node {
		return this.node;
	}

	getExpectedMime(): string {
		return this.node.getAttribute(MIME);
	}

	getLinkedUrl(): string {
		return this.node.getAttribute(RES_URL);
	}

	getReferer(): string {
		return this.node.getAttribute(FIRST_REF);
	}

	getProject(): Project {
		return this.project;
	}

	public async process() {
		this.markExistingNodeLinksAsUnused();
		this.response = await this.project.fetchUrl( this.getLinkedUrl(), this.getReferer() );
		this.debug('got resource', this.response.rawBody.length);
		this.decideIfValidResponse();

		//@TODO if a http fail occurs during parsing, the rest should still be written
		await this.parseBody();
		await this.postProcessing();
		await this.writeBody();
		this.node.setAttribute(DONE, true);
	}

	protected decideIfValidResponse(): boolean {
		if (!this.project.ignoreBadStatusCodes && this.response.statusCode !== 200) return false;
		if (this.response.rawBody.length === 0) throw new Error('empty body');
		return true;
	}

	protected markExistingNodeLinksAsUnused() {
		for (let link of this.node.getLinks(l => l.getFrom() === this.node && l.getAttribute(LINK_USED) === true)) {
			link.setAttribute(LINK_USED, false);
			// console.log('link inactive: ', this.node.getAttribute(RES_URL), ' -> ', link.getTo().getAttribute(RES_URL));
		}
	}

	protected async parseBody() {
		let mime = this.getExpectedMime();
		// this.debug('parse', mime);
		if (mime === 'text/html') {
			let newBody = await transformHtml(this, <string>this.response.body);
			this.response.rawBody = Buffer.from(newBody);
		} else if (mime === 'text/css') {
			let newBody = await transformCss(this, <string>this.response.body);
			this.response.rawBody = Buffer.from(newBody);
		}
	}

	protected async postProcessing() {
		for (let hook of this.project.getPostProcessingHooks(this.getLinkedUrl(), this.getExpectedMime())) {
			let output = await hook(this.response.rawBody, this.getLinkedUrl(), this.getExpectedMime(), this);
			if (output === null) continue;
			this.response.rawBody = Buffer.isBuffer(output) ? output : Buffer.from(output);
		}
	}

	protected async writeBody() {
		let path = this.node.getAttribute(PATH);
		if (!path) {
			path = this.calculateLocalPathFromUrl( this.getLinkedUrl(), this.getExpectedMime() );
			this.debug('#WARNING: no path for', path);
		}
		this.debug('now writing', path);
		await this.project.getStorage().save(path, this.response.rawBody);
		this.node.setAttribute(LAST_DOWNLOAD, this.project.getCurrentDownloadTime());
	}

	public setBaseUrl(url: string) {
		this.node.setAttribute(BASE_URL, url);
	}

	public setCanonicalUrl(url: string) {
		//@TODO implement
	}


	public async processResourceLink(url: string, fbmime: string, isResource?: boolean): Promise<string> {
		this.debug("processResourceLink",url, fbmime);
		if (url.substring(0,5) === 'data:' || url.substring(0,7) === 'mailto:' || url.substring(0,11) === 'javascript:') {
			return url;
		}
		const absolute = this.makeUrlAbsolute( url );
		const normalized = this.project.normalizeUrl( absolute );
		this.debug("absolute", absolute);
		let node = this.project.getState().getEntryFromUrl( normalized );
		if (!node) {
			//never seen before
			let maxDepth = this.node.getAttribute(MAX_DEPTH);
			node = this.project.getState().createEntry(normalized, this.getLinkedUrl(), typeof maxDepth === 'number' ? maxDepth-1 : null, isResource);
		} else if (!node.getAttribute(UPDATED_THIS_RUN)) {
			//seen in a previous run, allowed might have changed
			let maxDepth = this.node.getAttribute(MAX_DEPTH);
			this.project.getState().updateEntry(node, typeof maxDepth === 'number' ? maxDepth-1 : null, isResource);
		} else {
			//has already been seen this run
		}
		if (node === this.node) {
			return Path.basename(this.getLocalPath());
		}
		let attr = new Map<any, any>();
		let link = this.node.linkTo(node, attr, true);
		link.setAttribute(LINK_USED, true);
		// console.log('link active: ', this.node.getAttribute(RES_URL), ' -> ', node.getAttribute(RES_URL));
		if (node.getAttribute(ALLOWED) === true) {	//link to local or remote
			try {
				let local = await this.processToLocal(fbmime, node, absolute.hash);
				return local;
			} catch (e) {
				this.debug('error processing', e);
				node.setAttribute(FAILED, true);
				node.setAttribute(ALLOWED, false);
				return url;
			}
		} else {
			if (this.project.removeFilteredResource && node.getAttribute(MIME) !== 'text/html') {
				return '';
			}
			return absolute.href;
		}
	}

	protected async processToLocal(fbmime: string, node: Node, linkHash: string): Promise<string> {
		let mime = node.getAttribute(MIME);
		let url = node.getAttribute(RES_URL);
		this.debug('processToLocal', url);
		if (!mime) {
			mime = await this.project.lookupMime(url, fbmime, this.getLinkedUrl());
			node.setAttribute(MIME, mime);
		}
		let linkFile = node.getAttribute(PATH) || this.calculateLocalPathFromUrl( url, mime );
		let localFile = this.getLocalPath();
		let localUrl = this.calculateLocalUrl( linkFile, localFile, url, linkHash );
		this.project.getState().setPathForNode(node, linkFile);
		return localUrl;
	}

	/**
	 * get the url that we should use as basis to make urls absolute
	 * this is the url that we have opened
	 */
	getOpenUrl(): string {
		//@TODO fix
		return this.redirectUrl ? this.redirectUrl : this.getLinkedUrl();
	}

	/**
	 * used to make absolute urls, overridden from base-tag
	 */
	getBaseUrl(): string {
		return this.node.getAttribute(BASE_URL) ? this.node.getAttribute(BASE_URL) : this.getOpenUrl();
	}

	/**
	 * marks a redirect by the server
	 * @param {string} url
	 **/
	// setRedirectUrl( url ) {
	// 	this.redirectUrl = url;
	// 	if (this.project.linkRedirects) {	//@TODO more testing without links
	// 		this.addUrlToProject( url );
	// 	}
	// }



	/**
	 * create absolute url from relative link
	 **/
	makeUrlAbsolute( url: string ): URL {
		let baseUrl = this.getBaseUrl();
		this.debug('make absolute', baseUrl, url);
		let r = new URL( url, baseUrl );
		return r;
	}

	/**
	 * cache and get local path, generate if neccessary
	 **/
	getLocalPath(): string {
		if (!this._localPath) {
			this._localPath = this.expectedLocalPath ? this.expectedLocalPath
					: this.calculateLocalPathFromUrl( this.getLinkedUrl(), this.getExpectedMime() );
			this.debug("calculate local path: "+this._localPath);
		}
		return this._localPath;
	}

	/**
	 * create an absolute local path based on the project and the absolute url
	 */
	calculateLocalPathFromUrl( url: string, mime: string ): string {
		this.debug('calculateLocalPathFromUrl', { url, mime });
		let parsedUrl = new URL( url );
		parsedUrl.host = this.project.mapDomain(parsedUrl.host);
		let queryString = '';
		if (parsedUrl.search) {
			queryString = parsedUrl.search.replace(/[^a-z0-9_.]/ig,'_');	//alternative: only convert / to _
			if (queryString.length > 255) {	//workaround against too long file names
				queryString = Crypto.createHash('sha512').update(queryString).digest("base64");
			}
		}
		let ext = mime ? this.project.mime.getExtension( mime ) : Path.extname(url).substr(1);
		if (!ext) {
			this.debug("WARNING: unknown mime: "+mime+", falling back to html");
		}
		let ending = "." + (ext ? ext : 'html');
		let path = parsedUrl.pathname && parsedUrl.pathname.length > 1
					? parsedUrl.pathname : '/';
		path = decodeURI(path);
		if (path[path.length - 1] === '/') {
			path += this.project.defaultIndex;
		}
		let pathExt = Path.extname(path);
		if (pathExt && this.project.mime.getType(pathExt)) {	//extra check to not remove something like .2 at the end
			path = path.substring(0, path.length - pathExt.length);
		}
		path += queryString;
		path += ending;
		let full = Path.join( parsedUrl.hostname, path);
		this.debug("calculated local path to be ",{ full, queryString, ending });
		return full;
	}

	/**
	 * create a relative url between two local files
	 */
	calculateLocalUrl( linkPath: string, basePath: string, linkUrl: string, linkHash: string ): string {
		let linkParsed = new URL( linkUrl );
		let relPath = Path.relative( Path.dirname(basePath), Path.dirname(linkPath) );
		let relLink = Path.join( relPath, Path.basename( linkPath ) );
		let search = linkParsed.search ? linkParsed.search : '';
		this.debug("calc localUrl from ", { linkPath, linkUrl, basePath, relPath, relLink, search, linkHash });
		return relLink + search + linkHash;
	}


}
