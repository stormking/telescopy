import { URL } from "url";

const filterTypes = [
  "host",
  "path",
  "query",
  "port",
  "protocol",
  "auth",
  "hostname",
  "pathname",
  "hash",
];
const filterMap = {
  path: "pathname",
};
const compareTypes = ["=", "==", "===", ">", "<", "<=", ">=", "!=", "!=="];

export default function filter(filterJson) {
  let rules = filterJson.map(build);
  return function run(url: string): boolean {
    let parts = new URL(url);
    if (parts.search) {
      let query = {};
      for (let [k, v] of parts.searchParams.entries()) {
        query[k] = v;
      }
      Object.defineProperty(parts, "query", {
        get: () => query,
      });
    }
    for (let i = 0, ii = rules.length; i < ii; i++) {
      let fn = rules[i];
      let res = fn(parts);
      // console.log(parts, i, res);
      if (res !== null) {
        return res;
      }
    }
    return false;
  };
}

export type FilterRule = boolean | string | {
  type: string;
  key?: string;
  match?: any;
  nomatch?: any;
  test?: string | RegExp;
  comparison?: string;
  value?: string;
};

function build(filter: FilterRule | string | boolean): Function {
  if (typeof filter !== "object") return () => filter;

  if (!filter.type || filterTypes.indexOf(filter.type) === -1) {
    throw new Error("invalid filter.type: " + JSON.stringify(filter.type));
  }
  const type = filter.type in filterMap ? filterMap[filter.type] : filter.type;
  if (type === "query" && typeof filter.key !== "string") {
    throw new Error("filter.key must be a string, is " + (typeof filter.key));
  }
  if (
    typeof filter.match === "undefined" && typeof filter.nomatch === "undefined"
  ) {
    filter.match = true;
  }
  if (filter.test) {
    if (typeof filter.test === "object") {
      if (!filter.test.toString) {
        throw new Error("filter.test must be a regex");
      } else {
        filter.test = filter.test.toString();
      }
    }
    if (typeof filter.test !== "string") {
      throw new Error("filter.test must be a regex object or regex string");
    }
  }
  var numericalComparison = false;
  if (typeof filter.comparison !== "undefined") {
    if (compareTypes.indexOf(filter.comparison) === -1) {
      throw new Error("invalid comparison operator:" + filter.comparison);
    }
    if (/<|>/.test(filter.comparison)) {
      numericalComparison = true;
    }
  }
  if (filter.comparison === "=") filter.comparison = "===";
  var fnBody = '"use strict";';
  fnBody += `\nif(typeof url['${type}'] === 'undefined') return null;\n`;
  fnBody += `let value = url['${type}'];\n`;
  if (filter.key) {
    fnBody +=
      `if(typeof value['${filter.key}'] === 'undefined') return null;\n`;
    fnBody += `value = value['${filter.key}']\n`;
  }
  if (filter.test) {
    fnBody += "let testResult = " + filter.test + ".test(value);\n";
  } else {
    if (typeof filter.value !== "undefined") {
      let comp = filter.comparison || "===";
      if (!numericalComparison) {
        fnBody += `let testResult = (value ${comp} '${filter.value}');\n`;
      } else {
        fnBody += `let testResult = (1*value ${comp} 1*${filter.value});\n`;
      }
    } else {
      fnBody += `let testResult = true;\n`;
    }
  }
  if (typeof filter.match !== "undefined") {
    let res = typeof filter.match === "boolean"
      ? filter.match
      : '"' + filter.match + '"';
    fnBody += `if(testResult === true) return ${res};\n`;
  } else {
    let res = typeof filter.nomatch === "boolean"
      ? filter.nomatch
      : '"' + filter.nomatch + '"';
    fnBody += `if(testResult === false) return ${res};\n`;
  }
  fnBody += "return null;";
  // console.log(fnBody);
  return new Function("url", fnBody);
}
