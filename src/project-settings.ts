import { FilterRule } from "./filter"
import { Rule } from "./html-filters"
import Resource from "./resource"
import { Storage } from "./storage"

type MimeMap = { string: boolean }
export type PostProcessing = {
	mime?: string,
	urlFilter?: FilterRule[]|Function,
	hook: (input: Buffer, url: string, mime: string, resource: Resource) => Promise<null|Buffer|string>|null|Buffer|string
}

export default interface ProjectSettings {
	local: string,
	remote: string,
	cleanLocal?: boolean,
	cleanUnused?: boolean,
	tempDir?: string,
	skipExistingFiles?: boolean,
	skipExistingFilesExclusion?: object,
	maxRetries?: number,
	timeoutToHeaders?: number,
	timeoutToDownload?: number,
	linkRedirects?: boolean,
	defaultIndex?: string,
	userAgent?: string,
	proxy?: string,
	fetchHeaders?: { string: string },
	fetchCookies?: string[],
	domHook?: Function,
	htmlDomFilters?: { [x: string]: Rule },
	mimeDefinitions?: { string: string[] },
	mimeRules?: any,
	normalizeUrl?: Function,
	decideOnHeaders?: Function,
	domainMap?: string[][],
	baseWaitTime?: number,
	randWaitTime?: number,
	filterByUrl?: Function,
	urlFilter?: FilterRule[],
	storage?: 'file'|'memory'|Storage,
	skipFetchHeaders?: boolean,
	removeFilteredResource?: boolean,
	postProcessingHooks?: PostProcessing[],
	addIndex?: boolean,
	noDbCompress?: boolean,
	maxCrawlDepth?: number,
	retryDelay?: Function,
	httpTimeouts?: object,
	agentKeepAlive?: number,
	ignoreBadStatusCodes?: boolean,
	noDelayOnHeaders?: boolean,
	noUpdates?: boolean
}
