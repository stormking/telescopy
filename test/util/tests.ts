import * as cheerio from 'cheerio';
import Project from '../../src/project';

export async function getCheerio(f, project) {
	let buf = await project.getStorage().retrieve(f);
	const $ = cheerio.load(buf);
	return $;
}

export function getEventCounts(project: Project) {
	let counts = {
		resourceStart: 0,
		httpFetch: 0,
		httpHeader: 0
	};
	project.on('resourceStart', () => counts.resourceStart++);
	project.on('httpFetch', () => counts.httpFetch++);
	project.on('httpHeader', () => counts.httpHeader++);
	return () => counts;
}

process.on('unhandledRejection', err => {
	console.log('unhandledRejection', err);
});
process.on('uncaughtException', err => {
	console.log('uncaughtException', err);
});
