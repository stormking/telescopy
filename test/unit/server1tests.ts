import { getCheerio } from '../util/tests';
import { expect } from 'chai';
import { PATH } from '../../src/project-state';
import { Node } from '@stormking/datastructures';

export function addTests() {
	let project;
	it('has written the index file', async() => {
		let has = await project.getStorage().has('localhost/index.html');
		expect(has).to.be.true;
		let redir = await project.getStorage().retrieve('index.html');
		expect(redir.toString('utf8')).to.include('href="localhost/index.html"');
	});

	it('has set the right links in the index file', async() => {
		let $ = await getCheerio('localhost/index.html', project);
		let link = $('#l1');
		expect(link.length).to.equal(1);
		expect(link.attr('href')).to.equal('d/page1.html');
		link = $('#l2');
		expect(link.length).to.equal(1);
		expect(link.attr('href')).to.equal('d/page0.html');
		link = $('#l3');
		expect(link.length).to.equal(1);
		expect(link.attr('href')).to.equal('contact.html');
		link = $('#s1');
		expect(link.length).to.equal(1);
		expect(link.attr('src')).to.equal('media/script1.js');
	});

	it('has found bg1 in head style', async() => {
		let has = await project.getStorage().has('localhost/media/bg1.jpeg');
		expect(has).to.be.true;
	});

	it('has found bg2 in inline style', async() => {
		let has = await project.getStorage().has('localhost/media/bg2.jpeg');
		expect(has).to.be.true;
	});

	it('has found app.css in head', async() => {
		let has = await project.getStorage().has('localhost/media/app.css');
		expect(has).to.be.true;
	});

	it('has the direct link to app.css in index', async() => {
		let $ = await getCheerio('localhost/index.html', project);
		let link = $('link[rel=stylesheet]');
		expect(link.length).to.equal(1);
		expect(link.attr('href')).to.equal('media/app.css');
	});

	it('has the correct pathed link to app.css in page1', async() => {
		let $ = await getCheerio('localhost/d/page1.html', project);
		let link = $('link[rel=stylesheet]');
		expect(link.length).to.equal(1);
		expect(link.attr('href')).to.equal('../media/app.css');
	});

	it('has found normalize.css in app.css import', async() => {
		let has = await project.getStorage().has('localhost/media/normalize.css');
		expect(has).to.be.true;
	});

	it('has found bg.jpeg in app.css rule', async() => {
		let has = await project.getStorage().has('localhost/media/bg.jpeg');
		expect(has).to.be.true;
	});

	it('has found page3 from meta-refresh in page0', async() => {
		let has = await project.getStorage().has('localhost/d/page3.html');
		expect(has).to.be.true;
	});

	it('has found page2 link in page3', async() => {
		let has = await project.getStorage().has('localhost/d/page2.html');
		expect(has).to.be.true;
	});

	it('has created page2 with query from link in page1', async() => {
		let has = await project.getStorage().has('localhost/d/page2_date_20150512.html');
		expect(has).to.be.true;
	});

	it('has set the link to page2 with query in page1', async() => {
		let $ = await getCheerio('localhost/d/page1.html', project);
		let link = $('#p2');
		expect(link.length).to.equal(1);
		expect(link.attr('href')).to.equal('page2_date_20150512.html?date=20150512');
	});

	it('has created a link with hash', async() => {
		let $ = await getCheerio('localhost/d/page2.html', project);
		let link = $('#p1');
		expect(link.length).to.equal(1);
		expect(link.attr('href')).to.equal('page1.html#top');
	});

	it('has found contact2 from form.action', async() => {
		let has = await project.getStorage().has('localhost/contact2.html');
		expect(has).to.be.true;
	});

	it('has found contact3 from button.formaction', async() => {
		let has = await project.getStorage().has('localhost/contact3.html');
		expect(has).to.be.true;
	});

	it('has created contact_1.html from contact.php', async() => {
		let has = await project.getStorage().has('localhost/contact_1.html');
		expect(has).to.be.true;
		let $ = await getCheerio('localhost/contact_1.html', project);
		let link = $('h1');
		expect(link.length).to.equal(1);
		expect(link.text()).to.equal('Contact php');
		let mail = $('#mail');
		expect(mail.attr('href')).to.equal('mailto:contact@example.com');
	});

	it('has found icon2 from img.src', async() => {
		let has = await project.getStorage().has('localhost/media/icon"2".png');
		expect(has).to.be.true;
	});

	it('has not duplicated page1 from anchors', async() => {
		let all = project.getState().getAll();
		let filtered = all.filter((e: Node) => {
			let p = e.getAttribute(PATH);
			return p ? p.includes('page1') : false;
		});
		expect(filtered.count()).to.equal(1);
	})

	it('has saved the local path for a page', async() => {
		let node = project.getState().getEntryFromUrl('http://localhost:8080/index.html');
		expect(node.getAttribute(PATH)).to.equal('localhost/index.html');
	});
	return function setProject(p) {
		project = p;
	}
}
