# Dev Guide

## Node Attributes

- url (linked url)
- allowed (bool, cached filter result)
- path (local absolute path)
- done (bool, if file was already saved)
- mime (mime type after checking header)
- baseUrl (base href tag if exists, needed for creating outgoing links. might need to delete this before downloading the page bc it could change between mirrorings)
