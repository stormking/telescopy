import Resource from './resource';
import transformCss from './transform-css';
const debug = require('debug')('telescopy:resource:html:filter');

const builtIn = {

	//order is important, base.href must be on top
	'base.href': {
		selector: 'base[href]',
		action: 'exec',
		exec: (elem: cheerio.Cheerio ,resource: Resource) => {
			resource.setBaseUrl(elem.attr('href'));
			elem.remove()
		}
	},

	'a.href': {
		selector: 'a[href]',
		action: 'process',
		attribute: 'href',
		mime: 'text/html'
	},

	'area.href': {
		selector: 'area[href]',
		action: 'process',
		attribute: 'href',
		mime: 'text/html'
	},

	'iframe.src': {
		selector: 'iframe[src]',
		action: 'process',
		attribute: 'src',
		mime: 'text/html'
	},

	'link.canonical': {
		selector: 'link',
		filter: a => a.attr("rel") && a.attr("rel").toLowerCase() === 'canonical' && a.attr("href") && a.attr("href").length,
		action: 'exec',
		exec: (elem: cheerio.Cheerio, resource: Resource) => {
			let absolute = resource.makeUrlAbsolute( elem.attr('href') );
			resource.setCanonicalUrl( absolute.href );
			elem.remove();
		}
	},

	'link.stylesheet': {
		selector: 'link',
		filter: (a: cheerio.Cheerio) => a.attr('rel') && a.attr('rel').toLowerCase() === 'stylesheet' && a.attr('href') && a.attr('href').length,
		attribute: 'href',
		mime: 'text/css',
		resource: true
	},

	'img.src': {
		selector: 'img[src]',
		attribute: 'src',
		mime: 'image/jpeg',
		resource: true
	},

	'img.srcset': {
		selector: 'img[srcset]',
		action: 'exec',
		exec: async (elem: cheerio.Cheerio, resource: Resource) => {
			let res = [];
			let parts = elem.attr('srcset').split(',');
			for (let row of parts) {
				let parts = row.trim().split(' ');
				let url = parts[0].trim();
				parts[0] = await resource.processResourceLink( url, 'image/jpeg', true );
				res.push(parts.join(' '))
			}
			elem.attr('srcset',res.join(', '));
		}
	},

	'script.src': {
		selector: 'script[src]',
		attribute: 'src',
		mime: (a: cheerio.Element) => {
			return a.type ? 'application/'+a.type : 'application/javascript'
		},
		resource: true
	},

	'form.action': {
		selector: 'form[action]',
		attribute: 'action',
		mime: 'text/html'
	},

	'button.formaction': {
		selector: 'button[formaction]',
		attribute: 'formaction',
		mime: 'text/html'
	},

	'meta.http': {
		selector: 'meta',
		filter: (a: cheerio.Cheerio) => a.attr('http-equiv') && a.attr('http-equiv').toLowerCase() === 'refresh' && a.attr('content'),
		action: 'exec',
		exec: async(elem: cheerio.Cheerio, resource: Resource) => {
			let urlFound;
			elem.attr('content').replace( /^(\d+);url=(.+)$/i, function(all, time, url) {
				urlFound = url;
				return all;
			});
			if (urlFound) {
				let processed = await resource.processResourceLink( urlFound, 'text/html' );
				let replaced = elem.attr('content').replace(urlFound, processed);
				elem.attr('content', replaced);
			}
		}
	},

	'option.value': {
		selector: 'option[value]',
		filter: (a: cheerio.Cheerio) => a.attr('value') && a.attr('value').match(/https?\:/),
		attribute: 'value',
		mime: 'text/html'
	},

	'inlinestyle': {
		selector: '[style]',
		filter: (e: cheerio.Cheerio) => e.attr('style').match(/url\(/),
		action: 'exec',
		exec: async(elem: cheerio.Cheerio, resource: Resource) => {
			let style = elem.attr('style');
			style = await updateStyles(style,resource);
			elem.attr('style',style);
		}
	},

	'docstyle': {
		selector: 'style',
		action: 'exec',
		exec: async(elem: cheerio.Cheerio, resource: Resource) => {
			let style = elem.html();
			style = await updateStyles(style,resource);
			elem.html(style);
		}
	}
};

function updateStyles(text: string,resource: Resource) {
	debug("update inline style",text);
	return transformCss( resource, text );
};

export interface Rule {
	selector: string,
	action?: 'exec'|'process'|'remove',
	exec?: Function,
	mime?: string & Function,
	filter?: Function,
	attribute?: string,
	resource?: boolean
}

export default function build(userRules: { [x: string]: Rule }): Rule[] {
	let index: { [x: string]: Rule } = {};
	for (let k in builtIn) {
		index[k] = builtIn[k];
	}
	if (userRules) {
		for (let k in userRules) {
			index[k] = userRules[k];
		}
	}
	return Object.values(index).filter(entry => {
		if (typeof entry !== 'object') return false;
		// if (!(entry.tag && ((entry.target && entry.mime) || entry.exec))) {
		// 	throw new Error("invalidly configured html-attribute-filter: "+JSON.stringify(entry));
		// }
		return true;
	});
}
